import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { Highlight } from 'ngx-highlightjs';
import { HighlightLoader } from 'ngx-highlightjs';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule, FormsModule, Highlight],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'find-code-angular';
  fileContent: { name: string; content: string }[] = [];
  isAnalyzing: boolean = false;
  isSearching: boolean = false;
  canSearch = false
  query = ''
  matches:{id:string; metadata:{fileName:string}; score:string}[] = []

  private hjsLoader:HighlightLoader = Inject(HighlightLoader) 
  constructor(private http: HttpClient) {}

  onAppThemeChange(appTheme: 'dark' | 'light') {
    this.hjsLoader.setTheme(appTheme === 'dark' ? 'assets/styles/solarized-dark.css' : 'assets/styles/solarized-light.css');
  }

  handleFIleChange(event: Event) {
    console.log('ran');

    const filesObj = (event.target as HTMLInputElement).files;
    console.log(filesObj);

    if (filesObj) {
      const filesArray = Array.from(filesObj); // Convert to an array
      filesArray.forEach((file) => {
        const reader = new FileReader();
        reader.onload = () => {
          this.fileContent = [
            ...this.fileContent,
            { name: file.name, content: reader.result as string },
          ];
        };
        reader.readAsText(file);
        console.log(this.fileContent);
      });
    }
  }

  handleAnalyze() {
    this.isAnalyzing = true
    const res = this.http
      .post<{success:boolean}>('https://find-code-backend.onrender.com/api/v1/analyzeCode', this.fileContent)
      .subscribe({
        next:(analyzed) => {
          if(analyzed.success) {
            this.canSearch = true
            this.isAnalyzing = true
          }
        },
        error:() => {
          this.canSearch = false
          this.isAnalyzing = false
        },
        complete:() => {
          this.isAnalyzing = false
          this.canSearch = true
        }
      });
  }

  handleSearch() {
    if(!this.query) return
    this.isSearching = true
    this.http.get<{matches:{id:string; metadata:{fileName:string}; score:string}[]; success:boolean}>(`https://find-code-backend.onrender.com/api/v1/searchCode?query=${this.query}`).subscribe({
      next:(searchResults) => {
        if(searchResults.success) {
          this.matches = searchResults.matches
        }
        console.log(searchResults);
      },
      error:() => {
        this.isSearching = false
      },
      complete:() => {
        this.isSearching = false
      }
    })
  }
}
